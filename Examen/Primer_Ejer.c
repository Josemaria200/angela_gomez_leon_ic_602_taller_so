#include<stdio.h>

int main(){
	char x;
	char *p;

	char y;
	char *puntero;
	
	x = 'x';
	p = &x;
	
	printf("Contenido del puntero p = %c", *p);
	
	*p = *p + 1;
	printf("\nContenido del puntero p = %c", *p);

	*p = *p + 2;

	printf("\nContenido del puntero p = %c", *p);

	y = 'y';
	
	puntero = &y;

	printf("\nContenido del puntero 'puntero' = %c\n", *puntero);
}
