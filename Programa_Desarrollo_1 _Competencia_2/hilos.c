#include<stdio.h> //
#include<unistd.h> 
#include<pthread.h>//la libreria que permite para los hilos
#include<string.h>//para ocupar strlen

typedef struct dato 
{
	char *cadena;
	int x,y;
}parametro;

void gotoxy (int x, int y){
	printf("\033[%d;%df",y,x );
}

void *saludo(void *args){ //retornamos a void o a cualquier cosa
	parametro *par=(parametro *)args;
	int i;

	for (i = 0; i < strlen(par->cadena); ++i) //strlen para medir la longitud de la cadena
	{
		fflush(stdout);//nos va  dejar ir viendo el proceso 
		gotoxy(par->x,par->y);
		par->x++;//la incremete para que no borrelos caracteres y siga avanzandp en el eje x
		printf("%c",par->cadena[i]);
		sleep(1);//dormiremos el hilo secundario
	}
	printf("\n");
}
int main(int argc, char const *argv[])
{
	pthread_t hilo1,hilo2; //creamos las variables de tipo hilo
	parametro p1;
	p1.cadena="El proceso uno esta Reproducionedo musica";
	p1.x=03;
	p1.y=20;

	parametro p2;
	p2.cadena="El proceso dos esta escribiendo en word";
	p2.x=03;
	p2.y=21;


	parametro p3;
	p3.cadena="El proceso tres esta reproduciendo  un video";
	p3.x=03;
	p3.y=22;


	parametro p4;
	p4.cadena="El proceso tres esta transmitiendo imagenes";
	p4.x=03;
	p4.y=23;

pthread_create(&hilo1,NULL,saludo,(void *)&p1);	//en esta hacemos referencia a nuestro
//hilo y en el tercer parametro la funcion que va a ejecutar
//realizamos un type cast
pthread_create(&hilo2,NULL,saludo,(void *)&p2);
pthread_create(&hilo3,NULL,saludo,(void *)&p3);
pthread_create(&hilo4,NULL,saludo,(void *)&p4);
pthread_join(hilo1,NULL);//nos va a permitir ver el hilo que creamos 
pthread_join(hilo2,NULL);
pthread_join(hilo3,NULL);
pthread_join(hilo4,NULL);
	return 0;
}
